#version 400 core

in vec2 textureCoordinates;
in vec3 surface_normal;
in vec3 to_light_vector;
in vec3 to_camera_vector;
in float distantFogEntity_visibility;

out vec4 out_color;

uniform sampler2D blendMap;
uniform sampler2D mainTexture;
uniform sampler2D addonTexture1;
uniform sampler2D addonTexture2;
uniform sampler2D addonTexture3;

uniform vec3 light_color;
uniform vec3 distantFog_color;
uniform float reflectivity;
uniform float specular_damping;

void main(void)
{
    vec4 blendMapColor = texture(blendMap, textureCoordinates);
    float mainTextureCoef = 1 - (blendMapColor.r, blendMapColor.g, blendMapColor.b);

    vec2 tiledCoords = textureCoordinates + 40.0;

    vec4 mainTextureColor = texture(mainTexture, tiledCoords) * mainTextureCoef;
    vec4 addonTexture1Color = texture(addonTexture1, tiledCoords) * blendMapColor.r;
    vec4 addonTexture2Color = texture(addonTexture2, tiledCoords) * blendMapColor.g;
    vec4 addonTexture3Color = texture(addonTexture3, tiledCoords) * blendMapColor.b;

    vec4 finalColor = mainTextureColor + addonTexture1Color + addonTexture2Color + addonTexture3Color;

	vec3 unitNormal = normalize(surface_normal);
	vec3 unitToLight = normalize(to_light_vector);
	
	float normal_light_difference = dot(unitNormal, unitToLight);
	
	float brightness = max(normal_light_difference , 0.2);
	
	vec3 diffuse = brightness * light_color;
		
    out_color = (vec4(diffuse, 1.0) * finalColor);
//    out_color = mix(vec4(distantFog_color, 1.0), out_color, distantFogEntity_visibility);
}