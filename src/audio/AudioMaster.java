package audio;

import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.WaveData;

import java.util.ArrayList;
import java.util.List;

public class AudioMaster
{
    private static List<Integer> bufferIds = new ArrayList<>();

    public static void init()
    {
        try {
            AL.create();
        }catch (LWJGLException e)
        {
            System.out.print(e.getMessage());
        }

        setListener();
    }

    public static void setListener()
    {
        AL10.alListener3f(AL10.AL_POSITION,0,0,0);
        AL10.alListener3f(AL10.AL_VELOCITY,0,0,0);

    }

    public static int loadSound(String soundFile)
    {
        int bufferID = AL10.alGenBuffers();
        bufferIds.add(bufferID);
        WaveData waveData = WaveData.create(soundFile);
        AL10.alBufferData(bufferID, waveData.format, waveData.data, waveData.samplerate);
        waveData.dispose();
        return bufferID;
    }

    public static void cleanUp()
    {
        for(int id: bufferIds)
            AL10.alDeleteBuffers(id);
        AL.destroy();
    }
}
