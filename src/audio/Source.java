package audio;

import org.lwjgl.openal.AL10;

/**
 * Created by Ayman on 8/21/2018.
 */
public class Source {

    private int sourceID;

    public Source()
    {
        this.sourceID = AL10.alGenSources();
        setPosition(1,0,0);
        setPitch(1);
    }

    public void play(int bufferID)
    {
        AL10.alSourcei(this.sourceID, AL10.AL_BUFFER, bufferID);
        AL10.alSourcePlay(this.sourceID);
    }

    public void delete()
    {
        stop();
        AL10.alDeleteSources(this.sourceID);
    }

    public boolean isPlaying()
    {
        return AL10.alGetSourcei(sourceID, AL10.AL_SOURCE_STATE) == AL10.AL_PLAYING;
    }

    public void pause()
    {
        AL10.alSourcePause(sourceID);
    }

    public void stop()
    {
        AL10.alSourceStop(sourceID);
    }

    public void setVolume(int volume)
    {
        AL10.alSourcef(sourceID, AL10.AL_GAIN, volume);
    }

    public void setPitch(float pitch)
    {
        AL10.alSourcef(sourceID,AL10.AL_PITCH, pitch);
    }

    public void setLooping(boolean loop)
    {
        AL10.alSourcei(sourceID, AL10.AL_LOOPING, loop ? AL10.AL_TRUE : AL10.AL_FALSE);
    }
    public void setPosition(float x, float y, float z)
    {
        AL10.alSource3f(sourceID,AL10.AL_POSITION, x, y, z);
    }
}
