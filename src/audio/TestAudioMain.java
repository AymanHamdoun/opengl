package audio;
/**
 * Created by Ayman on 8/21/2018.
 */
public class TestAudioMain {
    private static Source source, source2;
    public static void playMusci()
    {
        AudioMaster.init();
        int soundBufferID = AudioMaster.loadSound("audio/bounce.wav");
        int sound2BufferID = AudioMaster.loadSound("audio/background/spyro.wav");
        source = new Source();
        source2 = new Source();

        source2.setLooping(true);
        source2.setVolume(1);
        source2.setPitch(1.0f);
//        source.play(soundBufferID);
        source2.play(sound2BufferID);

        float xPos = 1;
        int direction = 1;

        while(true)
        {
            xPos += direction * 0.1f;
            source2.setPosition(xPos,0,2);
            if(xPos < -12 || xPos > 12)
                direction *= -1;

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

//        source.delete();
//        AudioMaster.cleanUp();
    }
}
