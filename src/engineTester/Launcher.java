package engineTester;

import audio.AudioMaster;
import audio.Source;
import entities.Player;
import org.json.JSONObject;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import entities.Camera;
import entities.Entity;
import entities.Light;
import models.SpecularModel;
import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.OBJLoader;
import renderEngine.renderers.MasterRenderer;
import terrains.Terrain;
import textures.PlainTexture;
import textures.SpecularTexture;
import textures.TerrainTexture;
import textures.TerrainTexturePack;
import toolbox.ResourceManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Launcher 
{
    private static List<Entity> entities = new ArrayList<>();

    public static void main(String args[]) throws InterruptedException {
        DisplayManager displayManager = new DisplayManager();
        Loader loader = new Loader();
               
        displayManager.createDisplay();
        
        
        MasterRenderer masterRenderer = new MasterRenderer();


        
        Light light = new Light(
        							new Vector3f(566, 284, 67.75f),  // Position
        							new Vector3f(1,1, 1)		// Color
        						);

        Player player = new Player(
                new SpecularModel(
                        OBJLoader.loadObj("bunny", loader),
                        new SpecularTexture(loader.loadTexture("textures/solid/sky-blue"), 1, 5)
                ),
                new Vector3f(0, 0, 0),
                0, 0, 0,
                1
        );
        entities.add(player);
        Camera camera = new Camera(player);

        loadEntitiesFromFile("res/entities.json", loader);

        TerrainTexturePack texturePack = new TerrainTexturePack(
                new TerrainTexture(loader.loadTexture("materials/grass")),
                new TerrainTexture(loader.loadTexture("materials/rock_dark")),
                new TerrainTexture(loader.loadTexture("materials/concrete")),
                new TerrainTexture(loader.loadTexture("materials/rock_magic_blue"))
        );

        TerrainTexture blendMap = new TerrainTexture(loader.loadTexture("blendmaps/one"));
        Terrain terrain1 = new Terrain(0,0, blendMap, texturePack, loader, ResourceManager.getHeightmapPNG("terrain1"));


       // Open AL Test
		AudioMaster.init();
		int sound2BufferID = AudioMaster.loadSound("audio/background/bg1.wav");
		Source backgroundMusic = new Source();
		backgroundMusic.setLooping(true);
		backgroundMusic.setVolume(1);
		backgroundMusic.setPitch(1.0f);
		backgroundMusic.play(sound2BufferID);

		// Game Loop
        while (!Display.isCloseRequested() && !Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
        {
            player.move();
            camera.move();

            light.setPosition(new Vector3f(player.getPosition().x, player.getPosition().y + 10, player.getPosition().z));

            masterRenderer.processTerrain(terrain1);
            for(Entity entity: entities)
            {
                if (entity.getNote().equals("rotate"))
                    entity.rotate(0, 1, 0);
                masterRenderer.processEntity(entity);
            }
            masterRenderer.render(camera,  light);
                                  
            displayManager.updateDisplay();
            
            if(Keyboard.isKeyDown(Keyboard.KEY_L))
            {
            	System.out.println("Camera: X: "+camera.getPosition().x+", Y: "+camera.getPosition().y+", Z: "+camera.getPosition().z);
            	System.out.println("\nLight: X: "+light.getPosition().x+", Y: "+light.getPosition().y+", Z: "+light.getPosition().z);
            }
        }

        masterRenderer.cleanUp();
        loader.cleanMemory();
        displayManager.destroyDisplay();
    }

    public static void loadEntitiesFromFile(String fileLocation, Loader loader)
    {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileLocation));
            String line = "";
            while ((line = reader.readLine()) != null)
            {
                JSONObject jsonObject = new JSONObject(line);
                Entity entity = new Entity(
                        new SpecularModel(
                                OBJLoader.loadObj(jsonObject.getString("obj"), loader),
                                new SpecularTexture(loader.loadTexture(jsonObject.getString("texture")), jsonObject.getInt("reflectivity"), jsonObject.getFloat("specularDamping"))
                        ),
                        new Vector3f(jsonObject.getFloat("x"), jsonObject.getFloat("y"), jsonObject.getFloat("z")),
                        jsonObject.getFloat("rotateX") , jsonObject.getFloat("rotateY"), jsonObject.getFloat("rotateZ"),
                        jsonObject.getFloat("scale")
                );
                entity.getGeneralModel().setHasTransparency(jsonObject.getBoolean("hasTransparency"));
                entity.getGeneralModel().setUseFakeLighting(jsonObject.getBoolean("useFakeLighting"));
                if (jsonObject.getString("name").equals("dragon-1")) {
                    entity.setNote("rotate");
                }
                entities.add(entity);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
