package entities;

import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

public class Camera 
{
	private Vector3f position;
	
	private float pitch;
	private float yaw;
	private float roll;

	private float distanceFromPlayer = 60;
	private float angleAroundPlayer = 0;

	private Player player;
	
	public Camera(Player player)
	{
		this.player = player;
		position = new Vector3f();
	}
	
	public void move()
	{
		calculateZoom();
		calculatePitch();
		calculateAngleAroundPlayer();
		float dx = getXDistance();
		float dy = getYDistance();
		calculateCameraPosition(dx, dy);
		this.yaw = 180 - (player.getRotateY() + this.angleAroundPlayer);
	}

	public Vector3f getPosition() {
		return position;
	}

	public float getPitch() {
		return pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public float getRoll() {
		return roll;
	}

	public void setPitch(float pitch)
	{
		this.pitch = pitch;
	}

	public void setYaw(float yaw)
	{
		this.yaw = yaw;
	}

	public void setRoll(float roll)
	{
		this.roll = roll;
	}
	
	 public void calculateZoom()
	 {
	 	float zoomLevel = Mouse.getDWheel() * 0.1f;
	 	distanceFromPlayer -= zoomLevel;
	 }

	 private void calculatePitch()
	 {
	 	 if (Mouse.isButtonDown(0))
		 {
		 	float pitchOffset = Mouse.getDY() * 0.1f;
			this.pitch -= pitchOffset;
		 }
	 }

	 private void calculateAngleAroundPlayer()
	 {
	 	if (Mouse.isButtonDown(0))
		{
			float angleOffset = Mouse.getDX() * 0.3f;
			this.angleAroundPlayer -= angleOffset;
		}
	 }
	
	private float getXDistance()
	{
		return (float) (distanceFromPlayer * Math.cos(Math.toRadians(this.pitch)));
	}

	private float getYDistance()
	{
		return (float) (distanceFromPlayer * Math.sin(Math.toRadians(this.pitch)));
	}

	private void calculateCameraPosition(float dx, float dy)
	{
		float theta = player.getRotateY() + angleAroundPlayer;
		float offsetX = (float) (dx * Math.sin(Math.toRadians(theta)));
		float offsetZ = (float) (dx * Math.cos(Math.toRadians(theta)));
		position.y = player.getPosition().y + dy;
		position.x = player.getPosition().x - offsetX;
		position.z = player.getPosition().z - offsetZ;
	}
}
