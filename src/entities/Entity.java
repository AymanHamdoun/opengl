package entities;

import org.lwjgl.util.vector.Vector3f;

import models.GeneralModel;

public class Entity 
{
	private GeneralModel model;
	private Vector3f position;
	protected float rotateX, 
					rotateY, 
					rotateZ;
	
	private float scale;

	private String note;
	
	public Entity(GeneralModel model, Vector3f position, float rotateX, float rotateY, float rotateZ,
			float scale) {
		
		this.model = model;
		this.position = position;
		this.rotateX = rotateX;
		this.rotateY = rotateY;
		this.rotateZ = rotateZ;
		this.scale = scale;
		this.note = "";
	}
	
	public void move(float x, float y, float z)
	{
		position.x += x;
		position.y += y;
		position.z += z;
	}
	
	public void rotate(float rotateX, float rotateY, float rotateZ)
	{
		this.rotateX += rotateX;
		this.rotateY += rotateY;
		this.rotateZ += rotateZ;
	}

	public GeneralModel getGeneralModel() {
		return model;
	}

	public Vector3f getPosition() {
		return position;
	}

	public float getRotateX() {
		return rotateX;
	}

	public float getRotateY() {
		return rotateY;
	}

	public float getRotateZ() {
		return rotateZ;
	}

	public float getScale() {
		return scale;
	}


	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
