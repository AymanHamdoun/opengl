package entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import models.GeneralModel;
import renderEngine.DisplayManager;

public class FirstPersonCamera extends Entity
{
	private static final float FORWARD_SPEED = 20;
	private static final float TURN_SPEED = 20;
	
	private float currentSpeed;
	private float turnSpeed;
	

	public FirstPersonCamera(GeneralModel model, Vector3f position, float rotateX, float rotateY, float rotateZ,
			float scale)
	{
		super(model, position, rotateX, rotateY, rotateZ, scale);
	}
	
	public void update()
	{
		checkInput();
		
		float rotationY = turnSpeed * 0.01f;
		rotateY += rotationY;

		super.rotate(0, rotationY, 0);
		
		float distanceMoved = currentSpeed * DisplayManager.getFrameDuration();
		float distanceX = (float)(distanceMoved * Math.sin(rotateY));
		float distanceZ = (float)(distanceMoved * Math.cos(rotateY));
		
		if(Keyboard.isKeyDown(Keyboard.KEY_C))
		{
			System.out.println("OffsetX: "+distanceX +" OffsetZ: "+distanceZ+"\n");
		}
		
		super.move(distanceX,  0,  distanceZ);
		
		
	}
	
	private void checkInput()
	{
		if(Keyboard.isKeyDown(Keyboard.KEY_W))
			currentSpeed = FORWARD_SPEED;
		else if(Keyboard.isKeyDown(Keyboard.KEY_S))
			currentSpeed = -FORWARD_SPEED;
		else
			currentSpeed = 0;
		
		if(Keyboard.isKeyDown(Keyboard.KEY_D))
			turnSpeed = TURN_SPEED;
		else if(Keyboard.isKeyDown(Keyboard.KEY_A))
			turnSpeed = -TURN_SPEED;
		else
		{
			turnSpeed = 0;
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_C))
		{
			System.out.println("RotateX: "+rotateX +" RotateY: "+rotateY+" RotateZ: "+rotateZ);
		}
	}
	
}
