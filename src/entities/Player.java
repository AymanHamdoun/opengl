package entities;

import models.GeneralModel;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.DisplayManager;

public class Player extends Entity
{
    private static final float RUN_SPEED = 20;
    private static final float TURN_SPEED = 160;

    private float currentSpeed = 0;
    private float currentTurnSpeed = 0;

    public Player(GeneralModel model, Vector3f position, float rotateX, float rotateY, float rotateZ, float scale)
    {
        super(model, position, rotateX, rotateY, rotateZ, scale);
    }

    public void move()
    {
        checkInputs();
        super.rotate(0, currentTurnSpeed * DisplayManager.getFrameDuration(), 0);
        float distance = currentSpeed * DisplayManager.getFrameDuration();
        float dx = (float) (distance * Math.sin(Math.toRadians(getRotateY())));
        float dz = (float) (distance * Math.cos(Math.toRadians(getRotateY())));
        super.move(dx, 0, dz);
    }

    public void checkInputs()
    {
        if (Keyboard.isKeyDown(Keyboard.KEY_W))
            this.currentSpeed = Player.RUN_SPEED;
        else if (Keyboard.isKeyDown(Keyboard.KEY_S))
            this.currentSpeed = - Player.RUN_SPEED;
        else
            this.currentSpeed = 0;

        if (Keyboard.isKeyDown(Keyboard.KEY_D))
            this.currentTurnSpeed = - Player.TURN_SPEED;
        else if (Keyboard.isKeyDown(Keyboard.KEY_A))
            this.currentTurnSpeed = Player.TURN_SPEED;
        else
            this.currentTurnSpeed = 0;
    }
}
