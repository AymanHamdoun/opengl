package models;

public abstract class GeneralModel
{
    private int VAO_ID;
    private int vertextCount;
    private boolean isSpecular;
    private boolean hasTransparency;
    private boolean useFakeLighting;

    public GeneralModel(int vao_id, int vertextCount)
    {
        this.VAO_ID = vao_id;
        this.vertextCount = vertextCount;
        this.isSpecular = false;
        this.hasTransparency = false;
    }

    public int getVAO_ID() {
        return VAO_ID;
    }

    public int getVertextCount() {
        return vertextCount;
    }
    
    public boolean isSpecular() {return this.isSpecular;}
    
    public abstract Object getTexture();


    public boolean isHasTransparency() {
        return hasTransparency;
    }

    public void setHasTransparency(boolean hasTransparency) {
        this.hasTransparency = hasTransparency;
    }

    public boolean isUseFakeLighting() {
        return useFakeLighting;
    }

    public void setUseFakeLighting(boolean useFakeLighting) {
        this.useFakeLighting = useFakeLighting;
    }
}
