package models;

import textures.SpecularTexture;

public class SpecularModel extends GeneralModel
{
	SpecularTexture specularTexture;
		
	public SpecularModel(GeneralModel generalModel, SpecularTexture specularTexture)
	{
		super(generalModel.getVAO_ID(), generalModel.getVertextCount());
		this.specularTexture = specularTexture;
	}


	public SpecularTexture getTexture() {
		return specularTexture;
	}
}
