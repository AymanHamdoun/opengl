package models;

import textures.PlainTexture;

public class TexturedModel extends GeneralModel
{
	PlainTexture plainTexture;
		
	public TexturedModel(GeneralModel generalModel, PlainTexture plainTexture)
	{
		super(generalModel.getVAO_ID(), generalModel.getVertextCount());
		this.plainTexture = plainTexture;
	}

	public PlainTexture getTexture() {
		return plainTexture;
	}
	
}
