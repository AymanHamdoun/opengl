package renderEngine;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.*;

public class DisplayManager
{
    private final static int DEFAULT_WIDTH  = 1080;
    private final static int DEFAULT_HEIGHT = 720;
    private final static int FPS_LIMIT = 120;

    private final static String DEFAULT_TITLE = "OpenGL Tutorials";

    private ContextAttribs attribs;

    private int width, height;
    private int fps_limit;
    private String title;
    
    private long lastFrameTimeStamp;
    private static float lastFrameDuration;

    public DisplayManager()
    {
        width = DEFAULT_WIDTH;
        height = DEFAULT_HEIGHT;
        title = DEFAULT_TITLE;
        fps_limit = FPS_LIMIT;
    }

    public void createDisplay()
    {
        attribs = new ContextAttribs(3,2)
		        		.withForwardCompatible(true)
		        		.withProfileCore(true);

        try {
        	Display.setTitle(title); 
        	
            Display.setDisplayMode(new DisplayMode(width, height));
            //Display.setFullscreen(true);
        	Display.create(new PixelFormat(),attribs);
        } catch (LWJGLException e) {
            e.printStackTrace();
        }

        GL11.glViewport(0,0,width,height);
        lastFrameTimeStamp = getCurrentTimeMilliSeconds();
    }

    public void updateDisplay()
    {
        Display.sync(fps_limit);
        Display.update();
        long currentFrameTimeStamp = getCurrentTimeMilliSeconds();
        lastFrameDuration = (currentFrameTimeStamp - lastFrameTimeStamp)/1000f;
        lastFrameTimeStamp = getCurrentTimeMilliSeconds();
    }

    public void destroyDisplay()
    {
        Display.destroy();
    }
    
    private long getCurrentTimeMilliSeconds()
    {
    	return Sys.getTime() * 1000 / Sys.getTimerResolution();
    }
    
    public static float getFrameDuration()
    {
    	return lastFrameDuration;
    }

}
