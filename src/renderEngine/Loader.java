package renderEngine;

/*
*   To Draw a polygon, its data is stored in an array-like structure called Vertex Buffer Object (VBO)
*   A VBO is stored in a Vertex Array Object (VAO) which is passed to the GPU
*
*   To pass data to a vbo it should be bound
*   To pass data and VBOs to a VAO the VAO should be bound as well
*   when stop passing data unbind them
*
*   GL30 : VAO Class
*   GL15 : VBO Class
*/

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import models.GeneralModel;
import models.RawModel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

public class Loader
{
    private List<Integer> VAO_list = new ArrayList<>();
    private List<Integer> VBO_list = new ArrayList<>();
    private List<Integer> texture_list = new ArrayList<>();

    public GeneralModel loadToVAO(float[] verteces, int[] indeces, float[] textureCoords, float[] normals)
    {
        int VAO_ID = createAndBindVAO();

        createAndBindVBO(GL15.GL_ARRAY_BUFFER);
        	fillAndBindVBO(GL15.GL_ARRAY_BUFFER, 0, 3, verteces);
        unbindVBO(GL15.GL_ARRAY_BUFFER);
        
        createAndBindVBO(GL15.GL_ARRAY_BUFFER);
	    	fillAndBindVBO(GL15.GL_ARRAY_BUFFER, 1, 2, textureCoords);
	    unbindVBO(GL15.GL_ARRAY_BUFFER);
	    
	    createAndBindVBO(GL15.GL_ARRAY_BUFFER);
	    	fillAndBindVBO(GL15.GL_ARRAY_BUFFER, 2, 3, normals);
	    unbindVBO(GL15.GL_ARRAY_BUFFER);

        createAndBindVBO(GL15.GL_ELEMENT_ARRAY_BUFFER);
        
        GL15.glBufferData(
                GL15.GL_ELEMENT_ARRAY_BUFFER,
                toIntBuffer(indeces),
                GL15.GL_STATIC_DRAW
        );
        
        // We never unbind index buffer (GL_ELEMENT_ARRAY_BUFFER VBO) 
        
        unbindVAO();
        return new RawModel(VAO_ID,indeces.length);
    }
    
    public int loadTexture(String fileName)
    {
    	Texture texture = null;
    	try {
			texture = TextureLoader.getTexture("PNG", new FileInputStream("res/"+fileName+".png"));
			// Mipmapping Logic:
    	    GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
    	    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
    	    GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, -0.4f);
    	} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	int textureID = texture.getTextureID();
    	texture_list.add(textureID);
    	return textureID;
    }

    private int createAndBindVAO()
    {
        // Generate A VAO (Vertex Array Object) :
        int VAO_ID = GL30.glGenVertexArrays();

        // Bind The VAO :
        GL30.glBindVertexArray(VAO_ID);

        // Store its id in a list to keep track for later :
        VAO_list.add(VAO_ID);

        // Return The ID of the VAO
        return VAO_ID;
    }

    private int createAndBindVBO(int VBOType)
    {
        // Generate A VBO (Vertex Buffer Object) :
        int VBO_ID = GL15.glGenBuffers();

        // Bind The VBO to its type (vertex or index ...):
        GL15.glBindBuffer(VBOType, VBO_ID);

        // Store its id in a list to keep track for later :
        VBO_list.add(VBO_ID);

        // Return The ID of the VBO
        return VBO_ID;
    }


    private void fillAndBindVBO(int bufferType, int attributeIndex, Integer vertexSize, float[] data)
    {
        // Send data to currently bound VBO :
        GL15.glBufferData(
        		bufferType, 				// VBO of interest is of type GL15.GL_ARRAY_BUFFER
                toFloatBuffer(data),  		// Data to send to VBO
                GL15.GL_STATIC_DRAW   		// Data Sent will not be changed
        );

        // Bind VBO in VAO :
        GL20.glVertexAttribPointer(
                attributeIndex,     // Bind VBO to index attributeIndex in VAO,
                vertexSize,         // Each vertexSize elements belong to the same vertex
                GL11.GL_FLOAT,      // Each Data is of Type float (GL11.GL_FLOAT)
                false,				// normalized = false
                0,            		// there is 0 elements between each vertex
                0  					// start reading from the 0th element
        );
    }

    private void unbindVAO()
    {
        // Binding ZERO unbinds currently bound VAO
        GL30.glBindVertexArray(0);
    }

    private void unbindVBO(int VBOType)
    {
        // Binding ZERO unbinds currently bound VBO
        GL15.glBindBuffer(VBOType, 0);
    }

    private FloatBuffer toFloatBuffer(float[] data)
    {
        FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(data.length);
        floatBuffer.put(data);
        floatBuffer.flip();
        return floatBuffer;
    }

    private IntBuffer toIntBuffer(int[] data)
    {
        IntBuffer intBuffer = BufferUtils.createIntBuffer(data.length);
        intBuffer.put(data);
        intBuffer.flip();
        return intBuffer;
    }

    public void cleanMemory()
    {
        for(Integer i: VAO_list)
            GL30.glDeleteVertexArrays(i);

        for (Integer i: VBO_list)
            GL15.glDeleteBuffers(i);
        
        for(Integer i: texture_list)
        	GL11.glDeleteTextures(i);
    }
}
