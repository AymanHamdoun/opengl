/**
 * 
 */
package renderEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import models.GeneralModel;

/**
 * @author Ayman Hamdoun
 *
 */
public class OBJLoader
{
	
	private static List<Vector3f> verteces;
	private static List<Integer>  indeces;
	private static List<Vector2f> texture;
	private static List<Vector3f> normals;
	
	private static float[] vertexPositions, textureCoords, vertexNormals;
	private static int[] vertexIndeces;
	
	public static GeneralModel loadObj(String objFileName, Loader loader)
	{
		verteces = new ArrayList<>();
		indeces  = new ArrayList<>();
		texture  = new ArrayList<>();
		normals  = new ArrayList<>();
		
		vertexPositions = null;
		textureCoords   = null;
		vertexNormals   = null;
		vertexIndeces   = null;
		
		BufferedReader reader = null;
		try 
		{
			reader = new BufferedReader(new FileReader(new File("res/models/obj/"+objFileName+".obj")));
			
			String currentLine;
			String data[];
			while((currentLine = reader.readLine()) != null)
			{
				data = currentLine.split(" ");
				switch (data[0]) 
				{
					case "v":
						verteces.add(new Vector3f(Float.parseFloat(data[1]), Float.parseFloat(data[2]), Float.parseFloat(data[3])));
						break;
						
					case "vt":
						texture.add(new Vector2f(Float.parseFloat(data[1]), Float.parseFloat(data[2])));
						break;
						
					case "vn":
						normals.add(new Vector3f(Float.parseFloat(data[1]), Float.parseFloat(data[2]), Float.parseFloat(data[3])));
						break;
	
					default:
						break;
				}	
			}
			
			textureCoords = new float[verteces.size() * 2];
			vertexNormals = new float[verteces.size() * 3];
			
			reader = new BufferedReader(new FileReader(new File("res/models/obj/"+objFileName+".obj")));
			
			while((currentLine = reader.readLine()) != null)
			{
				data = currentLine.split(" ");
				if(data[0].equals("f"))
				{
					processVertex(data[1].split("/"));
					processVertex(data[2].split("/"));
					processVertex(data[3].split("/"));
				}
				
			}
			reader.close();
		} 
		catch (Exception e) 
		{
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		
		vertexPositions = new float[verteces.size() * 3];
		vertexIndeces   = new int[indeces.size()];
		
		int index = 0;
		for(Vector3f vertex: verteces)
		{
			vertexPositions[index++] = vertex.x;
			vertexPositions[index++] = vertex.y;
			vertexPositions[index++] = vertex.z;
		}
		
		index = 0;
		for(Integer vertexIndex: indeces)
			vertexIndeces[index++] = vertexIndex;
		
		
		
		
		return loader.loadToVAO(vertexPositions, vertexIndeces, textureCoords, vertexNormals);
	}
	
	// vertexData will be in the form of : "vertex-number texture-number normal-number" 
	private static void processVertex(String[] vertexData)
	{
		// since obj files index start from 1 not 0;
		int vertexNumber = Integer.parseInt(vertexData[0]) - 1; 
		
		// Start Indexing By adding this vertex as an index, in the end we end up with sequence of verteces to be connected
		indeces.add(vertexNumber);
		
		Vector2f currentTexture = texture.get(Integer.parseInt(vertexData[1]) - 1);
		textureCoords[vertexNumber * 2] = currentTexture.x;
		textureCoords[vertexNumber * 2 + 1] = 1 - currentTexture.y;
		
		Vector3f currentNormal = normals.get(Integer.parseInt(vertexData[2]) - 1);
		vertexNormals[vertexNumber * 3] = currentNormal.x;
		vertexNormals[vertexNumber * 3 + 1] = currentNormal.y;
		vertexNormals[vertexNumber * 3 + 2] = currentNormal.z;
	}

}
