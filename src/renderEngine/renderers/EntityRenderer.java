package renderEngine.renderers;

import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import entities.Entity;
import models.GeneralModel;
import shaders.StaticShader;
import textures.GeneralTexture;
import toolbox.GraphicsMath;

public class EntityRenderer
{
	private StaticShader staticShader;
	
	public EntityRenderer(StaticShader shader, Matrix4f projectionMatrix) 
	{		
		this.staticShader = shader;
		staticShader.start();
		staticShader.loadProjectionMatrix(projectionMatrix);
		staticShader.stop();
	}
	      
    public void render(Map<GeneralModel, List<Entity>> entities)
    {
    	List<Entity> entitiesToRender;
    	
    	for(GeneralModel model: entities.keySet())
    	{
    		prepareModel(model);
    		
    		entitiesToRender = entities.get(model);
    		
    		for(Entity entity: entitiesToRender)
    			renderEntity(entity);
    		
    		unbindTexturedModel();
    	}
    }
    
    private void renderEntity(Entity entity)
    {
    	prepareTransformationMatrix(entity);
    	
		GL11.glDrawElements(
				GL11.GL_TRIANGLES, 
				entity.getGeneralModel().getVertextCount(), 
				GL11.GL_UNSIGNED_INT, 
				0
			);
    }

    
    private void prepareModel(GeneralModel model)
    {
    	GL30.glBindVertexArray(model.getVAO_ID());
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);
        if (model.isHasTransparency()) {
        	MasterRenderer.disableCulling();
		}
        staticShader.loadFakeLighting(model.isUseFakeLighting());
        staticShader.loadShine( ((GeneralTexture) model.getTexture()).getReflectivity(), ((GeneralTexture) model.getTexture()).getSpecular_damping());
        
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D,  ((GeneralTexture) model.getTexture()).getTextureID());
    }
    
    
    private void prepareTransformationMatrix(Entity entity)
    {
        Matrix4f transformationMatrix = GraphicsMath.createTransformationMatrix(
        		entity.getPosition(), 
        		entity.getRotateX(), 
        		entity.getRotateY(), 
        		entity.getRotateZ(), 
        		entity.getScale()
    		);

        staticShader.loadTransformationMatrix(transformationMatrix);
	}
    
    private void unbindTexturedModel()
    {
        GL20.glDisableVertexAttribArray(2);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);
        MasterRenderer.enableCulling();
    }
   
}
