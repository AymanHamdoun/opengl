package renderEngine.renderers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;

import entities.Camera;
import entities.Entity;
import entities.Light;
import models.GeneralModel;
import org.lwjgl.util.vector.Vector3f;
import shaders.StaticShader;
import shaders.TerrainShader;
import terrains.Terrain;

public class MasterRenderer 
{
	private final float FIELD_OF_VIEW = 70;
	private final float NEAR_PLANE = 0.1f;
	private final float FAR_PLANE = 1000;

	public static final Vector3f DISTANT_FOG_COLOR = new Vector3f(0.52f, 0.8f, 0.92f);
	
	private Matrix4f projectionMatrix;
	
	// Shaders : 
	private StaticShader staticShader = new StaticShader();
	private TerrainShader terrainShader = new TerrainShader();
	
	// Renderers :
	private EntityRenderer entityRenderer;
	private TerrainRenderer terrainRenderer;
	
	private Map<GeneralModel, List<Entity>> entities = new HashMap<>();
	private List<Terrain> terrains = new ArrayList<>();
	
	public MasterRenderer()
	{
	 	createProjectionMatrix();
		
		entityRenderer = new EntityRenderer(staticShader, projectionMatrix);
		terrainRenderer = new TerrainRenderer(terrainShader, projectionMatrix);
	}
	
	public void processTerrain(Terrain terrain)
	{
		terrains.add(terrain);
	}
	
    public void processEntity(Entity entity)
    {
    	List<Entity> batch = entities.get(entity.getGeneralModel());
    	
    	if(batch == null)
    	{
    		List<Entity> newBatch = new ArrayList<>();
    		newBatch.add(entity);
    		entities.put(entity.getGeneralModel(), newBatch);
    	}
    	else
    		batch.add(entity);
    }
	
    public void prepare()
    {
    	MasterRenderer.enableCulling();
    	GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(DISTANT_FOG_COLOR.x, DISTANT_FOG_COLOR.y, DISTANT_FOG_COLOR.z, 1);
    }
	
	public void render(Camera camera, Light light)
	{
		prepare();
		
		staticShader.start();
		staticShader.loadDistantFogColor(MasterRenderer.DISTANT_FOG_COLOR);
		staticShader.loadLight(light);
		staticShader.loadViewionMatrix(camera); 
		entityRenderer.render(entities);
		staticShader.stop();
		
		terrainShader.start();
		terrainShader.loadDistantFogColor(MasterRenderer.DISTANT_FOG_COLOR);
		terrainShader.loadLight(light);
		terrainShader.loadViewionMatrix(camera);
		terrainRenderer.render(terrains);
		terrainShader.stop();
		
		entities.clear();
		terrains.clear();
	}
	
    private void createProjectionMatrix()
    {
    	float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
    	float yScale = (float) ((1f / Math.tan(Math.toRadians(FIELD_OF_VIEW / 2f))) * aspectRatio);
    	float xScale = yScale / aspectRatio;
    	float fustumLength = FAR_PLANE - NEAR_PLANE;
    	
    	projectionMatrix = new Matrix4f();
    	projectionMatrix.m00 = xScale;
    	projectionMatrix.m11 = yScale;
    	projectionMatrix.m22 = -((FAR_PLANE - NEAR_PLANE) / fustumLength);
    	projectionMatrix.m23 = -1;
    	projectionMatrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / fustumLength);
    	projectionMatrix.m33 = 0;
    }
	
	public void cleanUp()
	{
		staticShader.cleanMemory();	
		terrainShader.cleanMemory();
	}

	public static void enableCulling()
	{
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
	}

	public static void disableCulling()
	{
		GL11.glDisable(GL11.GL_CULL_FACE);
	}
}
