package renderEngine.renderers;

import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import shaders.TerrainShader;
import terrains.Terrain;
import textures.TerrainTexturePack;
import toolbox.GraphicsMath;

public class TerrainRenderer
{
	private TerrainShader terrainShader;
	
	public TerrainRenderer(TerrainShader terrainShader, Matrix4f projectionMatrix)
	{
		this.terrainShader = terrainShader;
	 	this.terrainShader.start();
		this.terrainShader.loadProjectionMatrix(projectionMatrix);
		this.terrainShader.connectTextureCoords();
		this.terrainShader.stop();
	}
	
	public void render(List<Terrain> terrains)
	{
		for(Terrain terrain : terrains)
		{
			prepareTerrain(terrain);
			renderTerrain(terrain);
			unbindTexturedModel();
		}
	}
	
    private void renderTerrain(Terrain terrain)
    {    	
    	prepareTransformationMatrix(terrain);
    	
		GL11.glDrawElements(
				GL11.GL_TRIANGLES, 
				terrain.getGeneralModel().getVertextCount(), 
				GL11.GL_UNSIGNED_INT, 
				0
			);
    }

    
    private void prepareTerrain(Terrain terrain)
    {
    	GL30.glBindVertexArray(terrain.getGeneralModel().getVAO_ID());
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);
                
		bindTextures(terrain);
    }

    private void bindTextures(Terrain terrain) {
		TerrainTexturePack terrainTexturePack = terrain.getTexturePack();

		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D,  terrainTexturePack.getMainTexure().getTextureID());

		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D,  terrainTexturePack.getAddonTexture1().getTextureID());

		GL13.glActiveTexture(GL13.GL_TEXTURE2);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D,  terrainTexturePack.getAddonTexture2().getTextureID());

		GL13.glActiveTexture(GL13.GL_TEXTURE3);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D,  terrainTexturePack.getAddonTexture3().getTextureID());

		GL13.glActiveTexture(GL13.GL_TEXTURE4);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D,  terrain.getBlendMap().getTextureID());
	}
    
    private void prepareTransformationMatrix(Terrain terrain)
    {
        Matrix4f transformationMatrix = GraphicsMath.createTransformationMatrix(
        		new Vector3f(terrain.getX(), 0, terrain.getZ()), 
        		0, 
        		0, 
        		0, 
        		1
    		);

        terrainShader.loadTransformationMatrix(transformationMatrix);
	}
    
    private void unbindTexturedModel()
    {
        GL20.glDisableVertexAttribArray(2);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);
    }
}
