package shaders;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;

public abstract class AbstractShader
{
    private int programID;
    private int vertexShaderID;
    private int fragmentShaderID;
    
    private static FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
    
    public AbstractShader(String vertexShader, String fragmentShader)
    {
        vertexShaderID = loadShader(vertexShader, GL20.GL_VERTEX_SHADER);
        fragmentShaderID = loadShader(fragmentShader, GL20.GL_FRAGMENT_SHADER);

        programID = GL20.glCreateProgram();
        GL20.glAttachShader(programID, vertexShaderID);
        GL20.glAttachShader(programID, fragmentShaderID);

        bindAttributes(); 
        GL20.glLinkProgram(programID);
        
        // For printing error purposes :
        GL20.glValidateProgram(programID); 
        
        getAllUniforms();
    }
    
    // Binding Attributes to shaders : 
    protected void bindAttribute(int attribute, String variableName)
    {
        GL20.glBindAttribLocation(programID, attribute, variableName);
    }
    
    protected abstract void bindAttributes();
    
    // Getting Uniform Variable Locations from shaders : 
    public int getUniformLocation(String uniformName)
    {
    	return GL20.glGetUniformLocation(programID,  uniformName);
	}
    
    protected abstract void getAllUniforms();

    public void loadInt(int location, int value) {
        System.out.println("loading: " + value + " in: "  + location);
        GL20.glUniform1i(location, value);
    }
    
    // Loading Uniform Variables to shaders : 
    protected void loadUniform1f(int location, float data)
    {
    	GL20.glUniform1f(location,  data);
    }
    
    public void loadUniformVec3(int location, Vector3f data)
    {
    	GL20.glUniform3f(location,  data.x, data.y, data.z);
    }
    
    protected void loadUniformBoolean(int location, boolean data)
    {
    	float _boolean = (float) (data ? 1 : 0);
    	GL20.glUniform1f(location, _boolean);
    }
    
    protected void loadMatrix4f(int location, Matrix4f matrix)
    {
    	matrix.store(matrixBuffer);
    	matrixBuffer.flip();
    	GL20.glUniformMatrix4(location, false, matrixBuffer);
    }
    
    // Shader Program Methods : 
    public void start()
    {
        GL20.glUseProgram(programID);
    }

    public void stop()
    {
        GL20.glUseProgram(0);
    }

    public void cleanMemory()
    {
        stop();
        GL20.glDetachShader(programID, vertexShaderID);
        GL20.glDetachShader(programID, fragmentShaderID);
        GL20.glDeleteShader(vertexShaderID);
        GL20.glDeleteShader(fragmentShaderID);
        GL20.glDeleteProgram(programID);
    }

    
    // Reading Shader Files Method : 
    private int loadShader(String file, int shaderType)
    {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;

            while ((line = bufferedReader.readLine()) != null)
                stringBuilder.append(line).append("\n");

            bufferedReader.close();
        }catch (IOException e)
        {
            e.printStackTrace();
        }

        int shaderID = GL20.glCreateShader(shaderType);
        GL20.glShaderSource(shaderID, stringBuilder);
        GL20.glCompileShader(shaderID);

        if(GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
            System.out.println(GL20.glGetShaderInfoLog(shaderID, 500));

        return shaderID;
    }
}
