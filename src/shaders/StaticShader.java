package shaders;

import org.lwjgl.util.vector.Matrix4f;

import entities.Camera;
import entities.Light;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.renderers.MasterRenderer;
import toolbox.GraphicsMath;

public class StaticShader extends AbstractShader {

    private static final String VERTEX_SHADER = "src/shaders/entityVertexShader.glsl";
    private static final String FRAGMENT_SHADER = "src/shaders/entityFragmentShader.glsl";
    
    private int location_transformation_matrix;
    private int location_projection_matrix;
    private int location_view_matrix;
    private int location_light_position;
    private int location_light_color;
    private int location_reflectivity;
	private int location_specular_damping;
	private int location_useFakeLighting;
	private int location_distanceFogColor;

    public StaticShader()
    {
        super(VERTEX_SHADER, FRAGMENT_SHADER);
    }  

    @Override
    protected void bindAttributes() 
    {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "in_textureCoordinates"); 
    }

	@Override
	protected void getAllUniforms() {
		location_transformation_matrix = getUniformLocation("transformation_matrix");
		location_projection_matrix = getUniformLocation("projection_matrix");
		location_view_matrix = getUniformLocation("view_matrix");
		location_light_position = getUniformLocation("light_position");
		location_light_color = getUniformLocation("light_color");
		location_reflectivity = getUniformLocation("reflectivity");
		location_specular_damping = getUniformLocation("specular_damping");
		location_useFakeLighting = getUniformLocation("useFakeLighting");
		location_distanceFogColor = getUniformLocation("distantFog_color");
	}

	public void loadDistantFogColor(Vector3f fogColor)
	{
		super.loadUniformVec3(location_distanceFogColor, fogColor);
	}

	public void loadFakeLighting(boolean useFakeLighting)
	{
		super.loadUniformBoolean(location_useFakeLighting, useFakeLighting);
	}
	
	public void loadLight(Light light)
	{
		loadUniformVec3(location_light_position, light.getPosition());
		loadUniformVec3(location_light_color, light.getColor());
	}
	
	public void loadShine(float reflective, float damping)
	{
		loadUniform1f(location_reflectivity,  reflective);
		loadUniform1f(location_specular_damping, damping);
	}
	
	public void loadTransformationMatrix(Matrix4f matrix)
	{
		loadMatrix4f(location_transformation_matrix, matrix);
	}
	
	public void loadProjectionMatrix(Matrix4f matrix)
	{
		loadMatrix4f(location_projection_matrix, matrix);
	}
	
	public void loadViewionMatrix(Camera camera)
	{
		loadMatrix4f(location_view_matrix, GraphicsMath.createViewMatrix(camera));
	}
}
