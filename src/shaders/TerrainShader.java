 package shaders;

import org.lwjgl.util.vector.Matrix4f;

import entities.Camera;
import entities.Light;
import org.lwjgl.util.vector.Vector3f;
import toolbox.GraphicsMath;

public class TerrainShader extends AbstractShader
{
    private static final String VERTEX_SHADER   = "src/shaders/terrainVertexShader.glsl";
    private static final String FRAGMENT_SHADER = "src/shaders/terrainFragmentShader.glsl";
    
    private int location_transformation_matrix;
    private int location_projection_matrix;
    private int location_view_matrix;
    private int location_light_position;
    private int location_light_color;
	private int location_distanceFogColor;

	private int location_mainTexture;
	private int location_addonTexture1;
	private int location_addonTexture2;
	private int location_addonTexture3;
	private int location_blendMap;

	public TerrainShader()
	{
		super(VERTEX_SHADER, FRAGMENT_SHADER); 
	}
  
    @Override
    protected void bindAttributes()
    {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "in_textureCoordinates");
    }

	@Override
	protected void getAllUniforms() {
		location_transformation_matrix = getUniformLocation("transformation_matrix");
		location_projection_matrix = getUniformLocation("projection_matrix");
		location_view_matrix = getUniformLocation("view_matrix");
		location_light_position = getUniformLocation("light_position");
		location_light_color = getUniformLocation("light_color");
		location_distanceFogColor = getUniformLocation("distantFog_color");

		location_mainTexture = getUniformLocation("mainTexture");
		location_addonTexture1  = getUniformLocation("addonTexture1");
		location_addonTexture2 = getUniformLocation("addonTexture2");
		location_addonTexture3 = getUniformLocation("addonTexture3");
		location_blendMap = getUniformLocation("blendMap");

	}

	public void connectTextureCoords() {
		loadInt(location_mainTexture, 0);
		loadInt(location_addonTexture1, 1);
		loadInt(location_addonTexture2, 2);
		loadInt(location_addonTexture3, 3);
		loadInt(location_blendMap, 4);
	}
	
	public void loadLight(Light light)
	{
		loadUniformVec3(location_light_position, light.getPosition());
		loadUniformVec3(location_light_color, light.getColor());
	}

	public void loadDistantFogColor(Vector3f fogColor)
	{
		super.loadUniformVec3(location_distanceFogColor, fogColor);
	}

	public void loadTransformationMatrix(Matrix4f matrix)
	{
		loadMatrix4f(location_transformation_matrix, matrix);
	}
	
	public void loadProjectionMatrix(Matrix4f matrix)
	{
		loadMatrix4f(location_projection_matrix, matrix);
	}
	
	public void loadViewionMatrix(Camera camera)
	{
		loadMatrix4f(location_view_matrix, GraphicsMath.createViewMatrix(camera));
	}
}
