#version 400 core

in vec2 textureCoordinates;
in vec3 surface_normal;
in vec3 to_light_vector;
in vec3 to_camera_vector;
in float distantFogEntity_visibility;

out vec4 out_color;

uniform sampler2D textureSampler;

uniform vec3 light_color;
uniform vec3 distantFog_color;
uniform float reflectivity;
uniform float specular_damping;

void main(void)
{
	vec3 unitNormal = normalize(surface_normal);
	vec3 unitToLight = normalize(to_light_vector);
	
	float normal_light_difference = dot(unitNormal, unitToLight);
	
	float brightness = max(normal_light_difference , 0.2);
	
	vec3 diffuse = brightness * light_color;
	
	vec3 unitToCamera = normalize(to_camera_vector);
	vec3 unitLightDirection = - unitToLight;	
	vec3 unitSpecularDirection = reflect(unitLightDirection, unitNormal);
	
	vec4 spec = vec4(0,0,0,1);
	
	if(specular_damping != -1 && reflectivity != -1)
	{
		float camera_specular = dot(unitSpecularDirection, unitToCamera);
		camera_specular = max(camera_specular, 0.0);
		
		float specular_factor = pow(camera_specular, specular_damping);
		vec3 specular_color = light_color * specular_factor;
		spec = vec4(specular_color, 1.0);
	}
	vec4 textureColor = texture(textureSampler, textureCoordinates);
	if (textureColor.a < 0.5) {
	    discard;
	}
    out_color = (vec4(diffuse, 1.0) * textureColor) + spec;
//    out_color = mix(vec4(distantFog_color, 1.0), out_color, distantFogEntity_visibility);
}