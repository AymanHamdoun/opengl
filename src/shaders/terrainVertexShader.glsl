#version 400 core

in vec3 position;
in vec2 in_textureCoordinates;
in vec3 normal_vector;

out vec2 textureCoordinates;
out vec3 surface_normal;
out vec3 to_light_vector;
out vec3 to_camera_vector;
out float distantFogEntity_visibility;

uniform mat4 transformation_matrix;
uniform mat4 projection_matrix;
uniform mat4 view_matrix;

uniform vec3 light_position;

const float distantFog_density = 0.007;
const float distantFog_gradient = 1.5;

void main(void)
{
	vec4 world_position = transformation_matrix * vec4(position, 1.0);
	vec4 positionRelativeToCamera = view_matrix * world_position;
    gl_Position = projection_matrix * positionRelativeToCamera;
    
    textureCoordinates = in_textureCoordinates;
	
	surface_normal  = (transformation_matrix * vec4(normal_vector, 0.0)).xyz;
	to_light_vector = light_position - world_position.xyz;
	to_camera_vector = (inverse(view_matrix) * vec4(0.0 , 0.0, 0.0, 0.0)).xyz - world_position.xyz;

    float distanceToCamera = length(positionRelativeToCamera.xyz);
    distantFogEntity_visibility = exp(-pow((distanceToCamera * distantFog_density), distantFog_gradient));
    distantFogEntity_visibility = clamp(distantFogEntity_visibility, 0.0, 1.0);
}