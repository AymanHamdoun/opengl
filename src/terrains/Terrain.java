package terrains;

import models.GeneralModel;
import org.lwjgl.Sys;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.Loader;
import textures.PlainTexture;
import textures.TerrainTexture;
import textures.TerrainTexturePack;
import toolbox.ResourceManager;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Terrain
{

	private final float SIZE = 800; 
	private int VERTEX_COUNT = 128;

	private static final int MAX_HEIGHT = 40;
	private static final int MAX_PIXEL_COLOR = 256 * 256 * 256;

	
	private float x, z;
	
	private GeneralModel generalModel;
	private TerrainTexturePack texturePack;
	private TerrainTexture blendMap;
	
	public Terrain(float x, float z, TerrainTexture blendMap, TerrainTexturePack terrainTexturePack, Loader loader, String heighMap)
	{
		this.x = x;
		this.z = z;
		this.blendMap = blendMap;
		this.texturePack = terrainTexturePack;
		this.generalModel = generateTerrain(loader, heighMap);
	}

	public TerrainTexturePack getTexturePack() {
		return texturePack;
	}

	public void setTexturePack(TerrainTexturePack texturePack) {
		this.texturePack = texturePack;
	}

	public TerrainTexture getBlendMap() {
		return blendMap;
	}

	public void setBlendMap(TerrainTexture blendMap) {
		this.blendMap = blendMap;
	}

	private GeneralModel generateTerrain(Loader loader, String heightMap)
	{
		BufferedImage heightMapImage = null;
		try {
			heightMapImage = ImageIO.read(new File(heightMap));
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		int VERTEX_COUNT = heightMapImage.getHeight();
		int count = VERTEX_COUNT * VERTEX_COUNT;
		float[] vertices = new float[count * 3];
		float[] normals = new float[count * 3];
		float[] textureCoords = new float[count*2];
		
		int[] indices = new int[6*(VERTEX_COUNT-1)*(VERTEX_COUNT-1)];
		
		int pointer = 0;
		
		for(int i = 0; i < VERTEX_COUNT; i++)
			for(int j = 0; j < VERTEX_COUNT; j++)
			{
				vertices[pointer * 3] = (float) j / ((float) VERTEX_COUNT - 1) * SIZE;
				vertices[pointer * 3 + 1] = getHeightFromPixel(heightMapImage, j, i);
				vertices[pointer * 3 + 2] = (float) i / ( (float) VERTEX_COUNT - 1) * SIZE;

				Vector3f normal = getNormalFromNeighbors(heightMapImage, j, i);
				normals[pointer*3] = normal.x;
				normals[pointer*3+1] = normal.y;
				normals[pointer*3+2] = normal.z;
				
				textureCoords[pointer*2] = (float) j / ( (float) VERTEX_COUNT - 1);
				textureCoords[pointer*2+1] = (float) i / ( (float) VERTEX_COUNT - 1);
				
				pointer++;
			}
		
		pointer = 0;
		for(int gz = 0; gz < VERTEX_COUNT - 1; gz++)
			for(int gx = 0; gx < VERTEX_COUNT-1; gx++)
			{
				int topLeft = (gz * VERTEX_COUNT) + gx;
				int topRight = topLeft + 1;
				int bottomLeft = ((gz+1) * VERTEX_COUNT) + gx;
				int bottomRight = bottomLeft + 1;
				
				indices[pointer++] = topLeft;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = topRight;
				indices[pointer++] = topRight;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = bottomRight;
			}
		
		return loader.loadToVAO(vertices, indices, textureCoords, normals);
	}

	public float getX()
	{
		return x;
	}

	public float getZ()
	{
		return z;
	}

	public GeneralModel getGeneralModel()
	{
		return generalModel;
	}
	
	public static float getHeightFromPixel(BufferedImage image, int x, int y)
	{
		if (x < 0 || y < 0 || x >= image.getHeight() || y >= image.getHeight())
			return 0;

		// Returns number between -MAX_PIXEL_COLOR and 0
		float height = image.getRGB(x, y);
		// Add this to make 0 the middle in the range
		height += MAX_PIXEL_COLOR/2f;
		// Divide so it can be between -1 & 1
		height /= MAX_PIXEL_COLOR/2f;
		// Now multiply by max height so it can be with our actual used range
		height *= MAX_HEIGHT;

		height += MAX_HEIGHT/4f;

		return height;
	}

	private Vector3f getNormalFromNeighbors(BufferedImage image, int x, int y)
	{
		float heightLeft = getHeightFromPixel(image, x - 1, y);
		float heightRight = getHeightFromPixel(image, x + 1, y);
		float heightDown = getHeightFromPixel(image, x, y - 1);
		float heightUp = getHeightFromPixel(image, x, y + 1);

		// MATH :: Theorem :: Finite Difference Method
		Vector3f normal = new Vector3f(heightLeft - heightRight, 2f, heightDown - heightUp);
		normal.normalise();

		return normal;
	}
	
}
