package textures;

public abstract class GeneralTexture 
{
	protected int textureID;
	
	protected float reflectivity = -1;
	protected float specular_damping = -1;
	
	public GeneralTexture(int id)
	{
		textureID = id;
	}
	
	public int getTextureID()
	{
		return textureID;
	}
	
	public float getReflectivity() {
		return reflectivity;
	}

	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}

	public float getSpecular_damping() {
		return specular_damping;
	}

	public void setSpecular_damping(float specular_damping) {
		this.specular_damping = specular_damping;
	}
}
