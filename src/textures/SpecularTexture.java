package textures;

public class SpecularTexture extends GeneralTexture
{
	public SpecularTexture(int id, float reflectivity, float specular_damping) 
	{
		super(id);
		if (reflectivity > 0) {
			this.reflectivity = reflectivity;
			this.specular_damping = specular_damping;
		}
	}
}
