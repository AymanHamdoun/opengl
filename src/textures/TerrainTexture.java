package textures;

public class TerrainTexture {

    private int textureID;

    public TerrainTexture(int id) {
        textureID = id;
        System.out.println("TextureID: " + textureID);
    }

    public int getTextureID() {
        return textureID;
    }

    public void setTextureID(int textureID) {
        this.textureID = textureID;
    }

}
