package textures;


public class TerrainTexturePack {

    private TerrainTexture mainTexure, addonTexture1, addonTexture2, addonTexture3;

    public TerrainTexturePack (TerrainTexture t1, TerrainTexture t2, TerrainTexture t3, TerrainTexture t4) {
        mainTexure = t1;
        addonTexture1 = t2;
        addonTexture2 = t3;
        addonTexture3 = t4;
    }
    public TerrainTexture getMainTexure() {
        return mainTexure;
    }

    public void setMainTexure(TerrainTexture mainTexure) {
        this.mainTexure = mainTexure;
    }

    public TerrainTexture getAddonTexture1() {
        return addonTexture1;
    }

    public void setAddonTexture1(TerrainTexture addonTexture1) {
        this.addonTexture1 = addonTexture1;
    }

    public TerrainTexture getAddonTexture2() {
        return addonTexture2;
    }

    public void setAddonTexture2(TerrainTexture addonTexture2) {
        this.addonTexture2 = addonTexture2;
    }

    public TerrainTexture getAddonTexture3() {
        return addonTexture3;
    }

    public void setAddonTexture3(TerrainTexture getAddonTexture3) {
        this.addonTexture3 = getAddonTexture3;
    }
}
