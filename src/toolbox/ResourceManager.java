package toolbox;

public class ResourceManager
{
    private final static String PATH_RESOURCE_FOLDER = "res/";
    private final static String PATH_HEIGHTMAPS_FOLDER = "heightmaps/";

    private final static String EXTENSION_PNG = ".png";

    public static String getResourcePNG(String imageName)
    {
        return PATH_RESOURCE_FOLDER + imageName + EXTENSION_PNG;
    }

    public static String getHeightmapPNG(String imageName)
    {
        return PATH_RESOURCE_FOLDER + PATH_HEIGHTMAPS_FOLDER + imageName + EXTENSION_PNG;
    }
}
