package world;

import org.lwjgl.util.vector.Vector3f;
import shaders.AbstractShader;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Atmosphere
{

    private boolean isFoggy;
    private float distantFogDensity;
    private float distantFogGradient;
    private Vector3f fogColor;

    private HashMap<String, Integer> uniformLocations;
    private List<AbstractShader> shaders;

    public Atmosphere(AbstractShader...shaders)
    {
        this.isFoggy = true;
        this.distantFogDensity = 0.007f;
        this.distantFogGradient = 1.5f;
        this.fogColor = new Vector3f(0.52f, 0.8f, 0.92f);
        this.shaders = Arrays.asList(shaders);
        uniformLocations = new HashMap<>();
        this.getAllUniformLocations();
    }

    public void implementInShaders()
    {

    }

    private void getAllUniformLocations()
    {

    }

    public boolean isFoggy() {
        return isFoggy;
    }

    public void setFoggy(boolean foggy) {
        isFoggy = foggy;
    }

    public float getDistantFogDensity() {
        return distantFogDensity;
    }

    public void setDistantFogDensity(float distantFogDensity) {
        this.distantFogDensity = distantFogDensity;
    }

    public float getDistantFogGradient() {
        return distantFogGradient;
    }

    public void setDistantFogGradient(float distantFogGradient) {
        this.distantFogGradient = distantFogGradient;
    }

    public Vector3f getFogColor() {
        return fogColor;
    }

    public void setFogColor(Vector3f fogColor) {
        this.fogColor = fogColor;
    }
}
